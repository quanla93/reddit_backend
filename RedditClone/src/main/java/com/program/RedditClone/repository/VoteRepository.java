package com.program.RedditClone.repository;

import com.program.RedditClone.model.Post;
import com.program.RedditClone.model.User;
import com.program.RedditClone.model.Vote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface VoteRepository extends JpaRepository<Vote, Long> {
    Vote findByPost(Post post);
    Optional<Vote> findTopByPostAndUserOrderByVoteIdDesc(Post post, User currentUser);
}
