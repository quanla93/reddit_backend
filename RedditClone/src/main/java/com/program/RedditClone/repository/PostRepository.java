package com.program.RedditClone.repository;

import com.program.RedditClone.model.Post;
import com.program.RedditClone.model.Subreddit;
import com.program.RedditClone.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {

    List<Post> findBySubreddit(Subreddit subreddit);
    List<Post> findByUser(User user);
    void deleteByPostId(Long id);

}
