package com.program.RedditClone.exceptions;

import org.springframework.mail.MailException;

public class SpringRedditException extends RuntimeException {
    public SpringRedditException(final String s) {
        super(s);
    }
}
