package com.program.RedditClone.service;

import com.program.RedditClone.dto.SubredditDTO;
import com.program.RedditClone.exceptions.SpringRedditException;
import com.program.RedditClone.mapper.SubredditMapper;
import com.program.RedditClone.model.Subreddit;
import com.program.RedditClone.repository.SubredditRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import java.util.stream.Collectors;

@Service
public class SubredditService {

    private final SubredditRepository subredditRepository;
    private final SubredditMapper subredditMapper;

    public SubredditService(final SubredditRepository subredditRepository
            , final SubredditMapper mapSubredditDto) {
        this.subredditRepository = subredditRepository;
        this.subredditMapper = mapSubredditDto;
    }

    @Transactional
    public SubredditDTO save(SubredditDTO subredditDTO){
       Subreddit subreddit = subredditRepository.save(subredditMapper.mapDtoToSubreddit(subredditDTO));
       subredditDTO.setId(subreddit.getId());
       return   subredditDTO;
    }

    @Transactional(readOnly = true)
    public List<SubredditDTO> getAll() {
        return subredditRepository.findAll()
                .stream()
                .map(subredditMapper::mapSubredditToDto)
                .collect(Collectors.toList());
    }

    public SubredditDTO getSubreddit(final Long id) {
        Subreddit subreddit = subredditRepository.findById(id)
                .orElseThrow(() -> new SpringRedditException(("No Subreddit found with " + id)));
        return subredditMapper.mapSubredditToDto(subreddit);
    }
}
