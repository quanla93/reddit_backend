package com.program.RedditClone.service;

import com.program.RedditClone.dto.PostRequest;
import com.program.RedditClone.dto.PostResponse;
import com.program.RedditClone.exceptions.SpringRedditException;
import com.program.RedditClone.mapper.PostMapper;
import com.program.RedditClone.model.Comment;
import com.program.RedditClone.model.Post;
import com.program.RedditClone.model.Subreddit;
import com.program.RedditClone.model.User;
import com.program.RedditClone.model.Vote;
import com.program.RedditClone.repository.CommentRepository;
import com.program.RedditClone.repository.PostRepository;
import com.program.RedditClone.repository.SubredditRepository;
import com.program.RedditClone.repository.UserRepository;
import com.program.RedditClone.repository.VoteRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class PostService {

    private final PostRepository postRepository;
    private final PostMapper postMapper;
    private final AuthService authService;
    private final SubredditRepository subredditRepository;
    private final UserRepository userRepository;
    private final VoteRepository voteRepository;
    private final CommentRepository commentRepository;

    public PostService(final PostRepository postRepository
            , final PostMapper postMapper
            , final AuthService authService
            , final SubredditRepository subredditRepository
            , final UserRepository userRepository, final VoteRepository voteRepository, final CommentRepository commentRepository) {
        this.postRepository = postRepository;
        this.postMapper = postMapper;
        this.authService = authService;
        this.subredditRepository = subredditRepository;
        this.userRepository = userRepository;
        this.voteRepository = voteRepository;
        this.commentRepository = commentRepository;
    }

    public Post save(final PostRequest postRequest) {
      Subreddit subreddit = subredditRepository
                .findByName(postRequest.getSubredditName())
                .orElseThrow(() -> new SpringRedditException("No found subreddit"));

        User user = authService.getCurrentUser();
       return postRepository.save(postMapper.map(postRequest, subreddit, user));
    }

    public List<PostResponse> getAllPosts() {
        return postRepository.findAll()
                .stream()
                .map(postMapper::mapToDto)
                .collect(Collectors.toList());
    }

    public PostResponse getPost(final Long id) {
        Post post = postRepository.findById(id)
                .orElseThrow(() -> new SpringRedditException("no found post"));

        return postMapper.mapToDto(post);
    }

    public List<PostResponse> getPostsBySubreddit(final Long supid) {
        Subreddit subreddit = subredditRepository.findById(supid)
                .orElseThrow(() -> new SpringRedditException("No found subreddit"));

        List<Post> posts = postRepository.findBySubreddit(subreddit);
        return posts
                .stream()
                .map(postMapper::mapToDto)
                .collect(Collectors.toList());
    }

    public List<PostResponse> getPostsByUsername(final String username) {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new SpringRedditException("no found user"));
        List<Post> posts = postRepository.findByUser(user);
        return posts.stream()
                .map(postMapper::mapToDto)
                .collect(Collectors.toList());
    }

    public Post update(final PostRequest postRequest) {
        Post post = postRepository.findById(postRequest.getPostId())
                .orElseThrow(() -> new SpringRedditException("no found post"));

        User user = authService.getCurrentUser();
        if (post.getUser().getUserId() == user.getUserId()){
            if (postRequest.getPostName() != null){
                post.setPostName(postRequest.getPostName());
            }
            if (postRequest.getDescription() != null) {
                post.setDescription(postRequest.getDescription());
            }
            if (postRequest.getUrl() != null) {
                post.setUrl(postRequest.getUrl());
            }
            if (postRequest.getSubredditName() != null) {
               Subreddit subreddit = subredditRepository.findByName(postRequest.getSubredditName())
                        .orElseThrow(() -> new SpringRedditException("no found subreddit"));
                post.setSubreddit(subreddit);
            }
        } else {
            throw new SpringRedditException("no correct user");
        }
        return postRepository.save(post);
    }

    public void delete(final Long id) {
        Post post = postRepository.findById(id)
                .orElseThrow(() -> new SpringRedditException("no found post"));
        User user = authService.getCurrentUser();
        if (user.getUserId() == post.getUser().getUserId()) {

            List<Comment> comments = commentRepository.findByPost(post);
            if (!comments.isEmpty()) {
                comments.stream().forEach(data -> {
                    commentRepository.delete(data);
                });
            }
            Vote vote = voteRepository.findByPost(post);
            if (vote != null) {
                voteRepository.delete(vote);
            }
            postRepository.deleteByPostId(post.getPostId());
        } else {
            throw new SpringRedditException("no correct user");
        }
    }
}
