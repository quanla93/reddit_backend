package com.program.RedditClone.service;

import com.program.RedditClone.dto.CommentsDTO;
import com.program.RedditClone.dto.PostRequest;
import com.program.RedditClone.dto.PostResponse;
import com.program.RedditClone.exceptions.SpringRedditException;
import com.program.RedditClone.mapper.CommentMapper;
import com.program.RedditClone.model.Comment;
import com.program.RedditClone.model.NotificationEmail;
import com.program.RedditClone.model.Post;
import com.program.RedditClone.model.Subreddit;
import com.program.RedditClone.model.User;
import com.program.RedditClone.repository.CommentRepository;
import com.program.RedditClone.repository.PostRepository;
import com.program.RedditClone.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CommentService {

    private final CommentRepository commentRepository;
    private final AuthService authService;
    private final PostRepository postRepository;
    private final CommentMapper commentMapper;
    private final UserRepository userRepository;
    private final MailContentBuilder mailContentBuilder;
    private final MailService mailService;

    public CommentService(final CommentRepository commentRepository
            , final AuthService authService
            , final PostRepository postRepository
            , final CommentMapper commentMapper
            , final UserRepository userRepository
            , final MailContentBuilder mailContentBuilder
            , final MailService mailService) {
        this.commentRepository = commentRepository;
        this.authService = authService;
        this.postRepository = postRepository;
        this.commentMapper = commentMapper;
        this.userRepository = userRepository;
        this.mailContentBuilder = mailContentBuilder;
        this.mailService = mailService;
    }


    public Comment save(final CommentsDTO commentsDto) {
        Post post = postRepository.findById(commentsDto.getPostId())
                .orElseThrow(() -> new SpringRedditException("no found post"));
        User user = authService.getCurrentUser();

        String message = mailContentBuilder.build(post.getUser().getUsername() + " posted a comment on your post." );
        sendCommentNotification(message, post.getUser());

        return commentRepository.save(commentMapper.map(commentsDto, post, user));
    }


    public List<CommentsDTO> getAllCommentsForPost(final Long postId) {
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new SpringRedditException("no found post"));
        List<Comment> comment = commentRepository.findByPost(post);

        return  comment.stream()
                .map(commentMapper::mapToDto)
                .collect(Collectors.toList());
    }

    public List<CommentsDTO> getAllCommentsForUser(final String userName) {
        User user = userRepository.findByUsername(userName)
                .orElseThrow(() -> new SpringRedditException("no found User"));
        List<Comment> comment = commentRepository.findByUser(user);

        return  comment.stream()
                .map(commentMapper::mapToDto)
                .collect(Collectors.toList());
    }

    private void sendCommentNotification(String message, User user) {
        mailService.sendMail(new NotificationEmail(user.getUsername() + " Commented on your post", user.getEmail(), message));
    }

    public void delete(final Long id) {
        Comment comment = commentRepository.findById(id)
                .orElseThrow(() -> new SpringRedditException("no found comment"));
        commentRepository.delete(comment);
    }

    public Comment update(final CommentsDTO commentsDTO) {
        Comment comment = commentRepository.findById(commentsDTO.getId())
                    .orElseThrow(() -> new SpringRedditException("no found post"));
        User user = authService.getCurrentUser();
            if (comment.getUser().getUserId() == user.getUserId()){
                if (commentsDTO.getText() != null){
                    comment.setText(commentsDTO.getText());
                }
            } else {
                throw new SpringRedditException("no correct user");
            }
            return commentRepository.save(comment);

    }


    public CommentsDTO getComment(final Long id) {
        Comment comment = commentRepository.findById(id)
                .orElseThrow(() -> new SpringRedditException("no found post"));

        return commentMapper.mapToDto(comment);
    }
}
